package com.faiz.user.dao;
import java.util.*;

import com.faiz.user.model.Emp;

import java.sql.*;


public class EmpDao {
	
	
	private static final String INSERT_Emp="insert into user911 (NAME,PASSWORD,EMAIL,COUNTRY) values (?,?,?,?);";
	private static final String UPDATE_Emp="update user911 set NAME=?, PASSWORD=?, EMAIL=?, country=? where id=?;";
	private static final String DELETE_Emp="delete from user911 where id=?;";
	private static final String SELECT_Emp_By_ID="select id,name,password,email,country from user911 where id=?;";
	private static final String SELECT_All_Emp="select * from user911;";
	
	
	
	
	public static int save(Emp em) {
		
		int status=0;
		try {
			MySqlConnection msc=new MySqlConnection();
			Connection con=msc.getConnection();
			PreparedStatement stmt=con.prepareStatement(INSERT_Emp);
			stmt.setString(1, em.getName());
			stmt.setString(2, em.getPassword());
			stmt.setString(3, em.getEmail());
			stmt.setString(4, em.getCountry());
			
			status=stmt.executeUpdate();
			
			
			con.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return status;
	}
	public static int Update(Emp em) {
		
		int status=0;
		try {
			MySqlConnection msc=new MySqlConnection();
			Connection con=msc.getConnection();
			PreparedStatement stmt=con.prepareStatement(UPDATE_Emp);
			stmt.setString(1, em.getName());
			stmt.setString(2, em.getPassword());
			stmt.setString(3, em.getEmail());
			stmt.setString(4, em.getCountry());
			stmt.setInt(5, em.getId());
			
			status=stmt.executeUpdate();
			
			
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	public static int delete(int id) {
		int status=0;
		try {
			MySqlConnection  msc=new MySqlConnection();
			Connection con=msc.getConnection();
			PreparedStatement stmt=con.prepareStatement(DELETE_Emp);
			stmt.setInt(1, id);
			
			status=stmt.executeUpdate();
			
			
			con.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return status;
	}
	public static Emp getEmpoyeeById(int id) {
		
		Emp em=new Emp();
		
		
		try {
			
			MySqlConnection msc=new MySqlConnection();
			Connection con=msc.getConnection();
			PreparedStatement stmt=con.prepareStatement(SELECT_Emp_By_ID);
			stmt.setInt(1, id);
			ResultSet rs=stmt.executeQuery();
			
			if(rs.next()) {
				em.setId(rs.getInt(1));
				em.setName(rs.getString(2));
				em.setPassword(rs.getString(3));
				em.setEmail(rs.getString(4));
				em.setCountry(rs.getString(5));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return em;
	}
	
	public static List<Emp> getAllEmployees(){
		
		List<Emp> list=new ArrayList<Emp>();
		
		
		try {
		
			MySqlConnection msc=new MySqlConnection();
			Connection con=msc.getConnection();
			PreparedStatement stmt=con.prepareStatement(SELECT_All_Emp);
			ResultSet rs=stmt.executeQuery();
			
			while(rs.next()) {
			
				Emp em=new Emp();
				
				em.setId(rs.getInt(1));
				em.setName(rs.getString(2));
				em.setPassword(rs.getString(3));
				em.setEmail(rs.getString(4));
				em.setCountry(rs.getString(5));
				
				list.add(em);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
