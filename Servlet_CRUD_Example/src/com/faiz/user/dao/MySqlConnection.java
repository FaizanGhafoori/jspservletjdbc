package com.faiz.user.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySqlConnection {
	private static String url="jdbc:mysql://localhost:3306/demo";
	private static String user="root";
	private static String password="ghafoori";
	
	public static Connection getConnection() {
		Connection con=null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(url,user,password);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return con;
	}
}
