package com.bsnl.user.model;

public class User {
	
	private int userid;
	private String first_name,last_name,email,address;
	
	public int getId() {
		return userid;
	}
	public void setId(int userid) {
		this.userid=userid;
	}
	
	public String getFirstName() {
		return first_name;
	}
	public void setFristName(String first_name) {
		this.first_name=first_name;
	}
	
	public String getLastNmae() {
		return last_name;
	}
	public void setLastName(String last_name) {
		this.last_name=last_name;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email=email;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address=address;
	}

}
