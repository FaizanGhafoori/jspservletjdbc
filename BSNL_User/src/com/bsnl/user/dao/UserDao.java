package com.bsnl.user.dao;

import java.sql.*;
import java.util.*;

public class UserDao {
	private String url="jdbc:mysql://localhost:3306/demo";
	private String user="root";
	private String pass="ghafoori";
	
	
	private static final String insert_User="insert into BSNL_User" + " (first_name, last_name, email, address) values "
	+"(?,?,?,?);";
	private static final String select_User_By_Id="select userid,first_name,last_name,email,address from BSNL_User where id=?";
	private static final String select_All_user="select * from BSNL_User";
	private static final String delete_User="delete from BSNL_User where id=?;";
	private static final String update_user="update BSNL_User set first_name=?,last_name=?,email=?,address=? where id=?;";

	public UserDao() {	
	}
	protected Connection getConnection() {
		Connection con=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(url,user,pass);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return con;
	}
	
}