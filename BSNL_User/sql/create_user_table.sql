use demo;

create table BSNL_User (
userid int primary key auto_increment not null ,
first_name varchar(30),
last_name varchar(30),
email varchar(20),
address varchar(50)
);
