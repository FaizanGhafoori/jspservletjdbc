package oops;

class Account{
	int acc_no;
	String name;
	float amount;
	void insert(int i, String n, float amt) {
		acc_no=i;
		name=n;
		amount=amt;
	}
	void deposit(float amt) {
		amount=amount+amt;
		System.out.println(amt+" Deposited");
	}
	void withdraw(float amt) {
		if(amount<amt) {
			System.out.println("Insufficient Balance");
		}else {
			amount=amount-amt;
			System.out.println(amt+" Withdrawn");
		}
	}
	void checkBalance() {
		System.out.println("Balance is: "+amount);
	}
	void display() {
		System.out.println(acc_no+" "+name+" "+amount);
	}
}
public class TestAccount {

	public static void main(String[] args) {
		Account ac=new Account();
		ac.insert(112055, "Faizan", 2000);
		ac.display();
		ac.checkBalance();
		ac.deposit(4000);
		ac.checkBalance();
		ac.withdraw(2000);
		ac.checkBalance();
		ac.withdraw(4000);
		ac.checkBalance();
	}

}
