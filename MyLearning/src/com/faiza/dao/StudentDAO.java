package com.faiza.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.faiza.to.Student;

public class StudentDAO {
	
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/family";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "ghafoori";
	   
	   public List<Student> getAllStudents(){
	   Connection conn = null;
	   Statement stmt = null;
	   List<Student> students = new ArrayList<>();
	   try{
	      conn = getConnection();
	      stmt = conn.createStatement();
	      String sql = "SELECT studentid, f_name, l_name, age, city FROM student ";
	      ResultSet rs = stmt.executeQuery(sql);
	      students = retrieveStudents(rs);
	      rs.close();
	   }catch(SQLException se){
	      //Handle errors for JDBC
	      se.printStackTrace();
	   }catch(Exception e){
	      //Handle errors for Class.forName
	      e.printStackTrace();
	   }finally{
	      //finally block used to close resources
	      try{
	         if(stmt!=null)
	            conn.close();
	      }catch(SQLException se){
	      }// do nothing
	      try{
	         if(conn!=null)
	            conn.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }//end finally try
	   }//end try
	   return students;
	}//end main

	private List<Student> retrieveStudents(ResultSet rs) throws SQLException {
		
		List<Student> students = new ArrayList<>();
		
		while(rs.next()){
			
			Student student  = new Student();
	         //Retrieve by column name
	         student.setStudentId(rs.getInt("studentid"));
	         student.setAge(rs.getInt("age"));
	         student.setFirstName(rs.getString("f_name"));
	         student.setLastname(rs.getString("l_name"));
	         student.setCity(rs.getString("city"));
	         students.add(student);
	      }
		return students;
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection conn;
	      Class.forName("com.mysql.jdbc.Driver");
	      conn = DriverManager.getConnection(DB_URL, USER, PASS);
		return conn;
	}
	}