package com.faiza.database;

import java.util.List;

import com.faiza.dao.StudentDAO;
import com.faiza.to.Student;

public class DataBaseLearning {

	public static void main(String[] args) {
		
		
		StudentDAO studentDAO =  new StudentDAO();
		
		List<Student> students = studentDAO.getAllStudents();

		for(Student student : students) {
	        System.out.print("ID: " + student.getStudentId());
	        System.out.print(", Age: " + student.getAge());
	        System.out.print(", First Name : " + student.getFirstName());
	        System.out.println(", Last Name: " + student.getLastname());
	        System.out.println(", City: " + student.getCity());
			
		}
	}

}
