package Instance_Initializer_Block;

/* 1. The instance initializer block is created when instance of the class is created.
   2. The instance initializer block is invoked after the parent class constructor is invoked 
      (i.e. after super() constructor call).
   3. The instance initializer block comes in the order in which they appear.
   
   */

class A {
	A(){
		System.out.println("parent class constructor invoked");
	}
}

public class Bike2 extends A {
	Bike2() {
		//super(); 		compiler created itself;
		System.out.println("child class constructor invoked");
	}
	
	{
		System.out.println("instance initializer block invoked");
	}

	public static void main(String[] args) {
		
		Bike2 bi=new Bike2();

	}

}
