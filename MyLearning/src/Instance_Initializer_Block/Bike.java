package Instance_Initializer_Block;

public class Bike {
	
	int speed;
	
	Bike() {
		System.out.println("Speed is "+speed);
		}
	{
		speed =100;		//Instance Initializer Block
	}
	
	public static void main(String[] args) {
		Bike b=new Bike();
		
	}

}
/* Instance Initializer Block is used to initialize the instance data member. It
  run each time when object of the class is created. */
