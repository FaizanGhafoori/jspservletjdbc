package Instance_Initializer_Block;

class A1 {
	A1() {
		System.out.println("parent class constructor invoked");
	}
}

	
	

public class Bike3 extends A1 {
	
	Bike3() {
		super();		//compiler create super() itself
		System.out.println("child class constructor invoed");
	}
	Bike3(int a) {
		super();		//compiler create super() itself
		System.out.println("child class parametrized constructor invoked");
	}
	
	{
		System.out.println("instance initializer block invoked");
	}
	
	public static void main(String[] args) {
		Bike3 bi=new Bike3();
		Bike3 bk=new Bike3(10);
	}
}
