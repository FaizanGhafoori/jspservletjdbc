package Instance_Initializer_Block;

public class Bike1 {

	int speed;
	Bike1() {
		System.out.println("Constructor invoked");
	}
	
	{
		System.out.println("Instance Initializer Block invoked");
	}
	
	public static void main(String[] args) {
		Bike1 b=new Bike1();
		Bike1 b1=new Bike1();

	}

}
/* In the above example, it seems that instance initializer block is firstly
invoked but NO. Instance initializer block is invoked at the time of object
creation. The java compiler copies the instance initializer block in the
constructor after the first statement super(). So firstly constructor is invoked.
let understand it in next class example*/