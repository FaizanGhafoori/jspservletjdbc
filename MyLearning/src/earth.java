import java.util.*;
class earth { 
 public static void main(String args[])
    {
        Scanner gh = new Scanner(System.in);
        System.out.println("Input the latitude of the cordinate 1:");
        double lat1 = gh.nextDouble();
        System.out.println("Input the longitude of the cordinate 2:");
        double lon1 = gh.nextDouble();
        System.out.println("Input the latitude of the cordinate 2:");
        double lat2 = gh.nextDouble();
        System.out.println("Input the longitude of the cordinate 2:");
        double lon2 = gh.nextDouble();
        
        System.out.println("Distance between those point on the earth's surface is: " + distance_on_earth(lat1, lon1, lat2, lon2) + " KM");
        
    }
    public static double distance_on_earth(double lat1, double lon1, double lat2, double lon2) {
        lat1 = Math.toRadians(lat1);
        lon1 = Math.toRadians(lon1);
        lat2 = Math.toRadians(lat2);
        lon2 = Math.toRadians(lon2);
        
        double radius = 6371.01; // KM
        return radius*Math.acos(Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon1-lon2));
    }
}