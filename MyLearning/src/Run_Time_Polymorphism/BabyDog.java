package Run_Time_Polymorphism;
class Animals {
	void eat() {
		System.out.println("eating");
	}
}
class Dogs extends Animals {
	void eat() {
		System.out.println("eating fruits");
	}
}
class BabyDog extends Dogs {
	
	void eat(){
		System.out.println("drinking milk");
	}

	public static void main(String[] args) {
		
		Animals a1,a2,a3;
		a1=new Animals();  	//a=new Animal();
		a1.eat();
		a2=new Dogs();		//a1=new Dog();
		a2.eat();
		a3=new BabyDog();	//a2=new BabyDog();
		a3.eat();

	}

}
