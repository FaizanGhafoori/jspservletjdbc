package Run_Time_Polymorphism;
class Bank {
	float getRateOfInterest() {
		return 0;
	}
}
class SBI extends Bank {
	float getRateOfInterest() {
		return 8.4f;
	}
}
class ICICI extends Bank {
	float getRateOfInterest() {
		return 7.3f;
	}
}
class Axis extends Bank {
	float getRateOfInterest() {
		return 9.7f;
	}
}
public class Bank_Poly {

	public static void main(String[] args) {
		
		Bank b;
		
		b=new SBI();		//upcasting
		System.out.println("Sbi Rate of Interest: "+b.getRateOfInterest());
		b=new ICICI();		//upcasting
		System.out.println("ICICI Rate of Interest: "+ b.getRateOfInterest());
		b=new Axis();		//upcasting
		System.out.println("Axis RAte of Interest: "+b.getRateOfInterest());

	}

}
