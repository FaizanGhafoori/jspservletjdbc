import java.util.*;
public class Factorial {

	public static void main(String[] args) {
		String f;
		do {
			Scanner s=new Scanner(System.in);
			System.out.println("Enter the factorial number: ");
			int n=s.nextInt();
			int fact=1;
			System.out.print(n+"!"+"=");
			for(int i=n; i>=1; i--) {
				System.out.print(i);
				fact=fact*i;
				if(i>1) {
					System.out.print("x");
				}
			}
			if(n>0) {
				System.out.println(" = "+fact);
			}else {
				System.out.println(fact);
			}
			System.out.println("Do you want to continue...(YES or NO)");
			f=s.next();
		 
	 }while(f.startsWith("y")||f.startsWith("Y"));
	}

}
