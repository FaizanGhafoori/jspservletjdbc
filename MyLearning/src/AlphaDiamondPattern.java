
public class AlphaDiamondPattern {

	public static void main(String[] args) {
		char[] a={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		for(int i=0;i<=14;i++) {
			for(int k=14;k>=i;k--) {
				System.out.print(" ");
			}
			for(int j=0;j<=2*i;j++) {
				if(i>0&&j>0&&j<2*i) {
					System.out.print(" ");
				}else {
					System.out.print(a[i]);
				}
			}
			System.out.println();			
		}
		for(int i=15;i>=0;i--) {
			for(int k=14;k>=i;k--) {
				System.out.print(" ");
			}
			for(int j=0;j<=2*i;j++) {
				if(i>0&&j>0&&j<2*i) {
					System.out.print(" ");
				}else {
					System.out.print(a[i]);
				}
			}
			System.out.println();			
		}
	}

}
