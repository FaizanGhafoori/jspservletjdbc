import java.util.Scanner;
public class PrimeNumbers {
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		String cn;
		do {
			System.out.println("Input start number...");
			int start=s.nextInt();
			System.out.println("Input end number...");
			int end=s.nextInt();
			for(int i=start; i<=end; i++) {
				int count=0;
				for(int j=2; j<=(i/2); j++) {
					if(i%j==0) {
						count=count+1;
						break;
					}
				}
				if(count==0&i!=1) {
					System.out.print(i+" ");
				}
			}
			System.out.println();
			System.out.println("Do you want to continue to find more prime numbers...(Press 'y' for YES or press 'n' for NO)");
			cn=s.next();
		}while(cn.equals("y")||cn.equals("Y"));
		
	}
}