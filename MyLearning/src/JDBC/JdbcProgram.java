package JDBC;
import java.sql.*;
import java.util.*;
public class JdbcProgram {

	public static void main(String[] args) {
		try {
//			Load the driver :
			
//			Class.forName("com.mysql.jdbc.Driver");

//			 Create a connection
			
			String url="jdbc:mysql://127.0.0.1:3306/family";
			String user="root";
			String password="ghafoori";
			Connection con=DriverManager.getConnection(url,user,password);
			
//			Create a query
			
			String q="select * from customer";
			
//			create a statement
			Statement stmt=con.createStatement();
			ResultSet rs=stmt.executeQuery(q);
//			rs.absolute(3);
//			rs.first();
			while(rs.next()) {
				System.out.println(rs.getInt("customer_id")+" "+rs.getString("cust_name")+" "+rs.getString("occupation")+" "+rs.getString("income")+" "+rs.getString("qualification"));
			}
			
//			System.out.println("table is created in database");
		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}

}
