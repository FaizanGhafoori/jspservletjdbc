package JDBC;
import java.sql.*;
public class ResultMetadataClass {

	public static void main(String[] args) {

		try {
			
			String url="jdbc:mysql://localhost:3306/family";
			String user="root";
			String password="ghafoori";
			Connection con=DriverManager.getConnection(url,user,password);
			
			String q="select * from customer";
			PreparedStatement stmt=con.prepareStatement(q);
			ResultSet rs=stmt.executeQuery();
			ResultSetMetaData rsmd=stmt.getMetaData();
			
			
			System.out.println("Total num of columns: "+rsmd.getColumnCount());
			System.out.println("1st Column name: "+rsmd.getColumnName(2));
			System.out.println("Type of first Column name: "+rsmd.getColumnTypeName(2));
			
			
		
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
