package JDBC;
import java.util.*;
import java.sql.*;
public class UpdateRecord {

	public static void main(String[] args) {
		try {
			// create the connection
			final String url="jdbc:mysql://localhost:3306/family";
			final String user="root";
			final String password="ghafoori";
			Connection con=DriverManager.getConnection(url,user,password);
			
			String q="update customer set cust_name=? where customer_id=?";
			String q1="select * from customer";
			PreparedStatement stmt=con.prepareStatement(q);
			PreparedStatement stm=con.prepareStatement(q1);
			stmt.setString(1, "Shuaib");
			stmt.setInt(2, 6);
			ResultSet rs=stm.executeQuery(q1);
			
			
			System.out.println(stmt.executeUpdate()+"record Updated");
			while(rs.next()) {
				System.out.println(rs.getInt("customer_id")+" "+rs.getString("cust_name")+" "+rs.getString("occupation")+" "+rs.getString("income")+" "+rs.getString("qualification"));
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
