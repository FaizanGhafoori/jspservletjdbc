package JDBC;
import java.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.*;
public class DelRecord {

	public static void main(String[] args) {
		try {
			
			final String url="jdbc:mysql://localhost:3306/family";
			final String user="root";
			final String password="ghafoori";
			
			Connection con=DriverManager.getConnection(url,user,password);
			
			String q="delete from customer where customer_id=?";
			PreparedStatement stmt=con.prepareStatement(q);
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			
			do {
				System.out.println("Enter customer_id:");
				int customer_id=Integer.parseInt(br.readLine());
				stmt.setInt(1, customer_id);

				int i=stmt.executeUpdate();
				System.out.println(i+"record deleted");
				
				System.out.println("DO you want to delete more record: y/n");
				String s=br.readLine();
				if(s.startsWith("n")) {
					break;
				}
				
			}while(true);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
