package JDBC;
import java.sql.*;
public class callableprac {

	public static void main(String[] args) {
		try {
//			Class.forName("com.mysql.jdbc.Driver");
			
			String url="jdbc:mysql://localhost:3306/family";
			String user="root";
			String password="ghafoori";
			
			Connection con=DriverManager.getConnection(url,user,password);
			
			CallableStatement stmt=con.prepareCall("{call insertR(?,?)}");
			
			stmt.setInt(1, 1011);
			stmt.setString(2, "Faizan");
			stmt.execute();
			System.out.println("Successful");
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
