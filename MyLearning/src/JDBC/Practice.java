package JDBC;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.*;
public class Practice {

	public static void main(String[] args) {
		try {
//			Class.forName("com.mysql.jdbc.Driver");
			
			String url="jdbc:mysql://localhost:3306/family";
			String user="root";
			String password="ghafoori";
			
			Connection con=DriverManager.getConnection(url,user,password);
			
			PreparedStatement stmt=con.prepareStatement("insert into user420 values(?,?,?,?)");
			
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			
			do {
				System.out.println("enter id:");
				int id=Integer.parseInt(br.readLine());
				
				System.out.println("Enter the user name:");
				String name=br.readLine();
				
				System.out.println("Enter the qualification:");
				String qualification=br.readLine();
				
				System.out.println("Enter the salary:");
				int salary=Integer.parseInt(br.readLine());
				
				
				stmt.setInt(1, id);
				stmt.setString(2, name);
				stmt.setString(3, qualification);
				stmt.setInt(4, salary);
				
				int i=stmt.executeUpdate();
				
				System.out.println(i+" record inserted");
				
				ResultSet rs=stmt.executeQuery("select * from user420");
				while(rs.next()) {
					System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getInt(4));
					
				}
				System.out.println("Do you want to inset more records? (y/n)");
				String s=br.readLine();
				if(s.startsWith("n")) {
					break;
				}
			}while(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

}
