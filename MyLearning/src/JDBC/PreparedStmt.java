package JDBC;
import java.util.*;
import java.sql.*;
import java.sql.DriverManager;

public class PreparedStmt {

	public static void main(String[] args) {
		try {
			//Load the driver Class;'
//			Class.forName("com.mysql.jdbc.Driver");
			
			//create connection
			String url="jdbc:mysql://localhost:3306/family";
			String user="root";
			String password="ghafoori";
			Connection con=DriverManager.getConnection(url,user,password);
			String q="insert into customer values(?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(q);
			stmt.setInt(1, 5);
			stmt.setString(2, "Faisal");
			stmt.setString(3, "Student");
			stmt.setString(4, "9000");
			stmt.setString(5, "B.C.A");
			
			
			System.out.println(stmt.executeUpdate()+ "record inserted");

		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}
}
