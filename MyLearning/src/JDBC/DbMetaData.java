package JDBC;
import java.sql.*;
import java.util.*;
public class DbMetaData {

	public static void main(String[] args) {
		try {
			
			String url="jdbc:mysql://localhost:3306/family";
			String user="root";
			String pasword="ghafoori";
			
			Connection con=DriverManager.getConnection(url,user,pasword);
			
			DatabaseMetaData dbmd=con.getMetaData();
			
			System.out.println("Driver Name: "+dbmd.getDriverName());
			System.out.println("Driver Version: "+dbmd.getDriverVersion());
			System.out.println("User Name: "+dbmd.getUserName());
			System.out.println("Database Product Name: "+dbmd.getDatabaseProductName());
			System.out.println("Database product Version: "+dbmd.getDatabaseProductVersion());
			
//			How to print total number of table;
			
			String table[]= {"TABLE"};	//or String table[]={"VIEW"};
			ResultSet rs=dbmd.getTables(null,null,null,table);
			
			while(rs.next()) {
				System.out.println(rs.getString(3));
			}
			
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
