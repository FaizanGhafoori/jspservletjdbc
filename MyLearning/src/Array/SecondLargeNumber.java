package Array;

public class SecondLargeNumber {

	public static void main(String[] args) {
		int[] a= {78,94,34,74,12,9,88,65,79,69};
//		int temp=0;
//		for(int i=0;i<a.length;i++) {
//			for(int j=i+1;j<a.length;j++) {
//				if(a[i]<a[j]) {
//					temp=a[i];
//					a[i]=a[j];
//					a[j]=temp;
//				}
//			}
//			if(i==1) {
//				break;
//			}
//		}
//		System.out.println(a[1]);
		int largest = Integer.MIN_VALUE;
		int second_largest = Integer.MIN_VALUE;
		for(int i=0;i<a.length;i++) {
			if(a[i]>largest) {
				second_largest = largest;
				largest = a[i];
			}else if(a[i]>second_largest && a[i]!=largest){
				second_largest = a[i];
			}
		}
		System.out.println(second_largest);
	}

}
