package Array;
import java.util.*;
public class Common_Ele_In_Two_Array2 {

	public static void main(String[] args) {
		int[] a= {12,45,65,77,12,55,31,32,65};
		int[] b= {56,89,78,77,12,26,31,77,86};		//when array have duplicate value in both array;
		HashSet<Integer> h=new HashSet<>();
		HashSet<Integer> s=new HashSet<>();
		for(int i:a) {
			h.add(i);		//put the element in h;
		}
		for(int i:b) {
			s.add(i);		//put the element in s;
		}
		for(int i:s) {
			boolean f=h.add(i);		//put the element of s into h;
			if(f==false) {
				System.out.print(i+" ");
			}
		}
	}

}
