package Array;

import java.util.*;

public class Array_Sorting {
	public static void sortArray(int []a , int length) {
		Arrays.sort(a);
		System.out.print("Sorting array in ascending order ");
		for(int i=0;i<length;i++) {
			System.out.print(a[i]+" ");
		}
		System.out.println();
		System.out.print("Sorting array in descending order ");
		for(int i=length-1;i>=0;i--) {
			System.out.print(a[i]+" ");
		}
		System.out.println();
	}
	public static void sortArray(String []a, int length) {
		Arrays.sort(a);
		System.out.println("Sorting array in ascending order "+Arrays.toString(a));
		
		Arrays.sort(a, Collections.reverseOrder());
		System.out.println("Sorting array in descending order "+Arrays.toString(a));
	}
	public static void sortArr(Integer []a, int length) {
		Arrays.sort(a);
		System.out.println("Sorting array in ascending order "+Arrays.toString(a));
		
		Arrays.sort(a, Collections.reverseOrder());
		System.out.println("Sorting array in descending order "+Arrays.toString(a));
	}
	
	public static void main(String[] args) {
		int [] arr= {15,24,28,75,45,36,95,17,26,61,38};
		sortArray(arr, arr.length);
		System.out.println();
		String [] ar= {"Faizan","Faisal","Shuaib","Suhail","Ateeb","Abdul Rehman","Hamza"};
		sortArray(ar, ar.length);
		System.out.println();
		Integer[] a= {25,24,78,75,95,34,16,92,37,46,29,85};
		sortArr(a, a.length);

	}

}
