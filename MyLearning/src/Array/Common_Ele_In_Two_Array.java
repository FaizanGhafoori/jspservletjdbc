package Array;
import java.util.*;
public class Common_Ele_In_Two_Array {
// by using for loop
	public static void main(String[] args) {
	 	int[] a= {1,5,9,7,6,4,89,56,48};
		int[] b= {21,1,3,5,6,8,9,7,88,75,7};
		HashSet<Integer> s=new HashSet<>(); //if element is duplicate we create an object of HashSet class.
		for(int i=0;i<a.length;i++) {
			for(int j=0;j<b.length;j++) {
				if(a[i]==b[j]) {
					s.add(a[i]);
					break;
				}
			}
		}
		for(int i:s) {
			System.out.print(i+" ");
		}

	}

}
