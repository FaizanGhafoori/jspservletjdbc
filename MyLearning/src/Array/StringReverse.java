package Array;

import java.util.Scanner;

public class StringReverse {
	String s="Ghafoori Inter College";
	void usingArray() {
		int c= s.length();
		char[] ch=new char[c];
		for(int i=0;i<c;i++) {
			ch[i]=(s.charAt(i));
		}
		System.out.print("Reverse of "+"\""+s+"\""+" is:- ");
		for(int i=c-1;i>=0;i--) {
			System.out.print(ch[i]);
		}
		System.out.println();
	}
	void usingCharAt() {
		String reverse="";
		for(int i=s.length()-1;i>=0;i--) {
			reverse=reverse+s.charAt(i);
		}
		System.out.println("Reverse of "+"\""+s+"\""+" is:- "+reverse);
	}
	void usingStringMethod() {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the text for revert");
		String name=sc.nextLine();
		char[] c=name.toCharArray();
		for(int i=c.length-1;i>=0;i--) {
			System.out.print(c[i]);
		}
		System.out.println();
	}
	void usingStrBuffer() {
		StringBuffer s=new StringBuffer("Faizan Ghafoori");
		System.out.println(s.reverse());
	}

	public static void main(String[] args) {
		StringReverse rev = new StringReverse();
		rev.usingArray();
		rev.usingCharAt();
		rev.usingStrBuffer();
		rev.usingStringMethod();
	}

}
