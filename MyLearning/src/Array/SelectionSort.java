package Array;

public class SelectionSort {

	public static void main(String[] args) {
		int[] a= {78,94,34,74,12,9,88,65,79,69};
		String[] b= {"Faizan","Suhail","Faisal","Ateeb","Maaz","Abdul","Hamza"};
		int temp=0;
		String flag="";
		for(int i=0;i<a.length;i++) {
			for(int j=i+1;j<a.length;j++) {
				if(a[j]<a[i]) {
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		for(int i=0;i<b.length;i++) {
			for(int j=i+1;j<b.length;j++) {
				if(b[j].compareTo(b[i])<0) {
					flag=b[i];
					b[i]=b[j];
					b[j]=flag;
				}
			}
		}
		for(int i=0;i<a.length;i++) {
			System.out.print(a[i]+" ");
		}
		System.out.println();
		for(int i=0;i<b.length;i++) {
			System.out.print(b[i]+" ");
		}
	}
}