package Array;
import java.util.*;
public class Common_Ele_In_Two_Array1 {

	public static void main(String[] args) {
		int[] a= {12,45,65,77,32,65}; 	//if both array have unique value;
		int[] b= {56,89,78,77,12,86};
		
		HashSet<Integer> h=new HashSet<>();
		for(int i:a) {
			h.add(i);
		}
		for(int i:b) {
			boolean f= h.add(i);
			if(f==false) {
				System.out.print(i+" ");
			}
		}

	}
}