package Array;

public class LinearSearch {

	public static void main(String[] args) {
		int[] a= {5,3,6,1,4,2,9,7};
		String[] b= {"Khalid","Hamza","Tooba","Ateeb","Aman"};
		String name="Khalid";
		int item=10;
		int flag=0;
		for(int i=0;i<a.length;i++) {
			if(a[i]==item) {
				System.out.println("Item is present at "+i+" index position");
				flag=1;
			}
		}
		if(flag==0) {
			System.out.println("Item is not present in the list..!");
		}
		for(int i=0;i<b.length;i++) {
			if(b[i].equals(name)) {
				System.out.println("Person is present at "+i+" index position.");
				flag=2;
			}
		}
		if(flag==0&&flag!=1) {
			System.out.println("Person is not present in the list..!");
		}
	}
}
