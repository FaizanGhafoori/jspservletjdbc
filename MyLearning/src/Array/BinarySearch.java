package Array;

public class BinarySearch {

	public static void main(String[] args) {
		int[] a = {78,94,34,74,12,9,88,65,79,69};
		
		int item = 79;
		int li = 0;
		int hi = a.length-1;
		int mi = (li+hi)/2;
		while(li<=hi) {
			if(item==a[mi]) {
				System.out.println("Item is at "+mi+" Index position..!");
				break;
			}else if(item<a[mi]){
				hi=mi-1;
			}else {
				li=mi+1;
			}
			mi=(li+hi)/2;
		}
	}
}
