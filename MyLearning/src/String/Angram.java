package String;

import java.util.Arrays;

public class Angram {

	public static void main(String[] args) {
		String a="Brag";
		String b="Grab";
		
		
		a=a.toLowerCase();
		b=b.toLowerCase();
		
		if(a.length()!=b.length()) {
			System.out.println("Both the string are not anagram");
		}else {
			char[] str=a.toCharArray();
			char[] strr=b.toCharArray();
			
			Arrays.sort(str);
			Arrays.sort(strr);
			
			if(Arrays.equals(str, strr)==true) {
				System.out.println("Both the string are anagram");
			}else {
				System.out.println("Both the string are not anagram");
			}
		}
		
	}

}
