import java.util.*;
public class FactorialByRecurction {
	
	public static void main(String[] args)
	{
		int a, fact;
		Scanner sc = new Scanner(System.in);
		System.out.println("Input Your No.");
		a =sc.nextInt();
		FactorialByRecurction ob=new FactorialByRecurction();
		fact=ob.calcFact(a);
		System.out.println("Factorial of "+a+" is = "+fact);
	}
	int calcFact(int a) 
	{
		if(a>=1) 
		{
			return(a*calcFact(a-1));
		}
		return 1;
	}

}
